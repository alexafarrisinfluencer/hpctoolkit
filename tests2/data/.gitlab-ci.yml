# Pipeline used to regenerate the cached test data
workflow:
  rules:
  - when: always

stages:
- generate
- publish

# Job template to regenerate some subset of the test data
.regen job:
  stage: generate
  needs:
  - pipeline: $PARENT_PIPELINE_ID
    job: 'deps: [amd64]'

  cache:
  - key: spack
    when: always
    paths: [.spack.git]
  - key: pkgs-$IMAGE_BASE
    when: always
    paths:
    - .pkg-cache/
  - key: ccache-$IMAGE_BASE$IMAGE_EXT-$JOB_CC
    when: always
    paths:
    - .ccache/

  variables:
    CCACHE_DIR: '$CI_PROJECT_DIR/.ccache'
    CCACHE_BASEDIR: '$CI_PROJECT_DIR'
    # Limit the ccache cache size to a modest level.
    # One HPCToolkit buildmany is ~2.5GB (~600MB compressed) over ~25000 files
    CCACHE_COMPRESS: 'true'
    CCACHE_MAXSIZE: '3G'
    CCACHE_MAXFILES: '30000'
    # Some of the test data is stored in a submodule
    GIT_SUBMODULE_STRATEGY: recursive
    # OpenMPI refuses to run as user 0 without these options set.
    OMPI_ALLOW_RUN_AS_ROOT: 1
    OMPI_ALLOW_RUN_AS_ROOT_CONFIRM: 1
  before_script:
  # Instantiate Spack
  - export SPACK_ROOT="$(realpath .spack)" PATH="$(realpath .spack)"/bin:"$PATH"
  - ci/dependencies/spack-init.sh
  script:
  - touch img-config
  # Generate a new batch of test data, saved to the logs/ dir
  - >-
    python3 -m ci.buildfe -l "logs/$CI_JOB_NAME" -a fresh-testdata-"$SUITE"
    img-config "ci/dependencies/latest;/spack/cenv/latest"
    --spack-no-install -1s "$SPEC"
  - mv logs/"$CI_JOB_NAME"/*/fresh-"$SUITE".tar.xz logs/fresh-"$SUITE".tar.xz

  artifacts:
    paths:
    - logs/
    when: always


'none':
  extends: .regen job
  tags: [docker, linux/amd64]
  image: $DEPS_IMAGES-$IMAGE_BASE-$ARCH
  variables:
    IMAGE_BASE: ubuntu20.04
    ARCH: amd64
    JOB_CC: gcc-10
    SPEC: '+tests2 +mpi ~debug +papi +python ~opencl ~cuda ~rocm ~level0'
    SUITE: none

'cpu':
  extends: .regen job
  tags: [docker, linux/amd64]
  image: $DEPS_IMAGES-$IMAGE_BASE-$ARCH
  variables:
    IMAGE_BASE: ubuntu20.04
    ARCH: amd64
    JOB_CC: gcc-10
    SPEC: '+tests2 +mpi ~debug +papi +python ~opencl ~cuda ~rocm ~level0'
    SUITE: cpu

'x86-64':
  extends: .regen job
  tags: [docker, linux/amd64]
  image: $DEPS_IMAGES-$IMAGE_BASE-$ARCH
  variables:
    IMAGE_BASE: ubuntu20.04
    ARCH: amd64
    JOB_CC: gcc-10
    SPEC: '+tests2 +mpi ~debug +papi +python ~opencl ~cuda ~rocm ~level0'
    SUITE: x86-64

'nvidia':
  extends: .regen job
  image: $DEPS_IMAGES-ubuntu20.04-cuda11.6.2-$ARCH
  tags: [docker, linux/amd64, gpu/nvidia/usrspace:11.6, gpu/nvidia>6.0]
  variables:
    IMAGE_BASE: ubuntu20.04
    IMAGE_EXT: +cuda
    ARCH: amd64
    JOB_CC: gcc-9
    SPEC: '+tests2 +mpi ~debug +papi +python ~opencl +cuda ~rocm ~level0'
    SUITE: nvidia
  before_script:
  - !reference [.regen job, before_script]
  # Pull CUDA from the image itself
  - echo '--with-cuda=/usr/local/cuda' > img-config

'sw-cuda':
  extends: .regen job
  image: $DEPS_IMAGES-ubuntu20.04-cuda11.8.0-$ARCH
  tags: [docker, linux/amd64]
  variables:
    IMAGE_BASE: ubuntu20.04
    IMAGE_EXT: +cuda
    ARCH: amd64
    JOB_CC: gcc-9
    SPEC: '+tests2 +mpi ~debug +papi +python ~opencl +cuda ~rocm ~level0'
    SUITE: sw-cuda
  before_script:
  - !reference [.regen job, before_script]
  # Pull CUDA from the image itself
  - echo '--with-cuda=/usr/local/cuda' > img-config

'amd':
  extends: .regen job
  image: $DEPS_IMAGES-ubuntu20.04-rocm5.2.3-$ARCH
  tags: [docker, linux/amd64, gpu/amd/usrspace:5.2]
  variables:
    IMAGE_BASE: ubuntu20.04
    IMAGE_EXT: +rocm
    ARCH: amd64
    JOB_CC: gcc-9
    SPEC: '+tests2 +mpi ~debug +papi +python ~opencl ~cuda +rocm ~level0'
    SUITE: amd
  before_script:
  - !reference [.regen job, before_script]
  # Pull ROCm from the image itself
  - echo '--with-rocm=/opt/rocm' > img-config


package:
  stage: publish
  image: docker.io/alpine
  tags: [docker]
  variables:
    GIT_SUBMODULE_STRATEGY: none
  script:
  - apk add xz
  - mkdir /tmp/result
  # Unpack all the tarballs to the temporary directory
  - tar x -af logs/fresh-none.tar.xz -C /tmp/result
  - tar x -af logs/fresh-cpu.tar.xz -C /tmp/result
  - tar x -af logs/fresh-nvidia.tar.xz -C /tmp/result
  - tar x -af logs/fresh-amd.tar.xz -C /tmp/result
  - tar x -af logs/fresh-x86-64.tar.xz -C /tmp/result
  - tar x -af logs/fresh-sw-cuda.tar.xz -C /tmp/result
  # Repack into a single tarball
  - tar c -Jf testdata.tar.xz -C /tmp/result .
  artifacts:
    expose_as: "Regenerated test data"
    paths:
    - testdata.tar.xz
