# NB: This is a crazy pipeline to dodge fundamental flaws in Spack:
#   - `spack ci` doesn't work on multiple environments. This means we have to
#     generate one pipeline per environment concretization.
#   - Spack has to be gently massaged / heavily tainted to cross-concretize correctly.
#     This means operations spanning multiple OSs, like concretization, need to be
#     spread out across multiple jobs and containers... and thus pipelines.
#   - Spack environments are unable to unify specs in a stable and controlled manner,
#     either all specs are concretized together (and views are possible), or
#     some to none are (and views are likely to fail). So we need multiple envionments
#     to properly express our testing dependencies.
#
# In short, if done the correct and straightforward way, we would have a combinatorial
# explosion of jobs and pipelines in this file. To avoid that, we use a "Multi-Level Pipeline" (MLP)
# approach. We let `spack ci generate` generate a boatload of pipelines, but then use an automated
# script to generate a single top-level pipeline that launches all the others. This removes most of
# the combinatorial burden, leaving us with (two) trigger jobs to launch the MLP.

# Generate a pipeline for each combination of OS and arch.
'deps:generate: [amd64]':
  stage: deps:generate
  image: registry.gitlab.com/hpctoolkit/ci-images/$IMAGE:$ARCH
  tags: [docker, linux/$ARCH]
  needs: []
  rules: !reference [.rebuild images, rules]
  variables:
    ARCH: amd64
  parallel: &parallel_amd64
    matrix:
    - IMAGE: leap15
      CI_SPACK_CC: gcc@7
    - IMAGE: almalinux8
      CI_SPACK_CC: gcc@8
    - IMAGE: ubuntu20.04
      CI_SPACK_CC: gcc@9
    - IMAGE: fedora36
      CI_SPACK_CC: gcc@12

  cache:
  - key: spack
    when: always
    paths: [.spack.git]

  script:
  # Instantiate Spack
  - export SPACK_ROOT="$(realpath .spack)" PATH="$(realpath .spack)"/bin:"$PATH"
  - ci/dependencies/spack-init.sh
  # Determine the generic target to use in Spack-speak
  - |
    case "$ARCH" in
    amd64) CI_SPACK_ARCH=x86_64_v3; ;;
    arm64) CI_SPACK_ARCH=aarch64; ;;
    *)
      echo "Invalid ARCH: $ARCH"
      exit 1
      ;;
    esac
    echo "Will concretize for generic target $CI_SPACK_ARCH"
  # Setup the Spack configuration for concretization:
  #   Always concretize to the generic target instead of a specific one
  - spack config --scope site add "packages:all:target:['$CI_SPACK_ARCH']"
  #   Always concretize to a particular compiler if there are multiple
  - >-
    if test -n "$CI_SPACK_CC"; then
    spack config --scope site add "packages:all:compiler:['$CI_SPACK_CC']" || exit $?;
    fi
  # Set up concrete environments named by the OS and ARCH
  - mkdir -p ci/dependencies/c/$IMAGE/$ARCH/minimum/
  - cp ci/dependencies/minimum/spack.yaml ci/dependencies/c/$IMAGE/$ARCH/minimum/
  - mkdir -p ci/dependencies/c/$IMAGE/$ARCH/latest/
  - cp ci/dependencies/latest/spack.yaml ci/dependencies/c/$IMAGE/$ARCH/latest/

  - &postjob
    - ci/scripts/network-stats.sh .netstats.txt

  # Concretize each of the environments in turn, into subdirectories named by the OS and ARCH
  - >-
    echo -e 'minimum\nlatest' | xargs -P0 -I '{}' -t
    spack -e "ci/dependencies/c/$IMAGE/$ARCH/{}/" concretize --reuse
  # Load the downstream buildcaches, so the secrets are available
  - ci/dependencies/spack-bc-downstream.sh > .bc-ds-mirrors.yaml
  - spack config --scope site add -f .bc-ds-mirrors.yaml
  - rm .bc-ds-mirrors.yaml
  # Generate pipelines for each of the environments
  # Generate the single pipeline for this combination
  - >-
    echo -e "minimum\nlatest" | xargs -P0 -I '{}' -t
    spack -e "ci/dependencies/c/$IMAGE/$ARCH/{}/" ci generate
    --optimize
    --check-index-only
    --buildcache-destination "$(spack mirror list | grep -m1 '^destination ' | awk '{print $2}')"
    --artifacts-root "spack-ci-artifacts/$IMAGE/$ARCH/{}/"
    --output-file "spack-ci-artifacts/$IMAGE/$ARCH/{}/ci.yml"
  # Annotate the pipeline with the source job, for the MLP
  - >-
    echo -e "\n...\n---\nsource: {pipeline: \"$CI_PIPELINE_ID\", job: \"$CI_JOB_NAME\", name: \"$IMAGE $ARCH\"}"
    | tee -a spack-ci-artifacts/$IMAGE/$ARCH/{minimum,latest}/ci.yml

  - *postjob

  artifacts:
    paths:
    - ci/dependencies/c/*/*/*/spack.lock
    - spack-ci-artifacts/

'deps:generate: [arm64]':
  extends: 'deps:generate: [amd64]'
  variables:
    ARCH: arm64
  parallel: &parallel_arm64
    matrix:
    # - IMAGE:
    #   - almalinux8
    #   - fedora36
    - IMAGE: ubuntu20.04
      CI_SPACK_CC: gcc@9


# Construct a Multi-Level Pipeline that runs all the pipelines from spack ci generate
'deps:mlp: [amd64]':
  stage: deps:prep
  tags: [docker]
  image: registry.gitlab.com/hpctoolkit/ci-images/ubuntu20.04:latest
  rules: !reference [.rebuild images, rules]
  needs:
  - 'deps:generate: [amd64]'
  before_script:
  - python3 -m pip install ruamel.yaml
  script:
  - >-
    ci/dependencies/mlp ci.yml
    spack-ci-artifacts/{almalinux8,fedora36,leap15,ubuntu20.04}/amd64/{minimum,latest}/ci.yml
  artifacts:
    paths:
    - ci.yml
'deps:mlp: [arm64]':
  extends: 'deps:mlp: [amd64]'
  needs:
  - 'deps:generate: [arm64]'
  script:
  - >-
    ci/dependencies/mlp ci.yml
    spack-ci-artifacts/ubuntu20.04/arm64/{minimum,latest}/ci.yml


# Launch the MLP and get this done.
deps:build amd64:
  stage: deps:build
  rules: !reference [.rebuild images, rules]
  needs: ['deps:mlp: [amd64]']
  trigger:
    strategy: depend
    include:
    - job: 'deps:mlp: [amd64]'
      artifact: ci.yml
deps:build arm64:
  stage: deps:build
  rules: !reference [.rebuild images, rules]
  needs: ['deps:mlp: [arm64]']
  trigger:
    strategy: depend
    include:
    - job: 'deps:mlp: [arm64]'
      artifact: ci.yml
